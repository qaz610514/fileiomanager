﻿namespace com.FunJimChee.Jason.FileIo
{
    using System;
    using System.IO;
    using Newtonsoft.Json;
    using UnityEngine;

    public static class FileIOManager
    {
        public static (bool result, T data) LoadFile<T>(string path, string key) where T : class, new()
        {
            try
            {
                var text = File.ReadAllText($"{path}/{key}.json");

                var data = JsonConvert.DeserializeObject<T>(text);

                return (true, data);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                return (false, default);
            }
        }

        public static bool SaveFile<T>(string path, string key, T data) where T : class, new()
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                var json = JsonConvert.SerializeObject(data);
                
                File.WriteAllText($"{path}/{key}.json", json);

                return true;
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                return false;
            }
        }

        public static void DeleteFile(string path, string key)
        {
            File.Delete($"{path}/{key}.json");
        }
    }

}