﻿namespace com.FunJimChee.Jason.FileIo.Example
{
    using UnityEngine;

    public class Demo : MonoBehaviour
    {
        class DemoClass
        {
            public string Name;
            public string Message;
        }

        // Start is called before the first frame update
        void Start()
        {
            var path = $"{Application.dataPath}/Jason/FileIOManager/Example";

            //Save Data
            var data = new DemoClass {Name = "yoyo", Message = "我好兇～"};
            FileIOManager.SaveFile(path, "Example", data);

            //Load Data
            var (result, getData) = FileIOManager.LoadFile<DemoClass>(path, "Example");
            Debug.Log(result ? $"{result} => {getData.Name} say : {getData.Message}" : $"Load fail!");

            //Delete Data
            //FileIOManager.DeleteFile(path, "Example");
        }
    }
}